We use the [<%= group %> Workflow issue board](https://gitlab.com/groups/gitlab-org/-/boards/<%= board_id %>?milestone_title=%23upcoming) to track what we work on in the current milestone.

Development moves through workflow states in the following order:

 1. `workflow::design` (if applicable)
 1. `workflow::planning breakdown` 
 1. `workflow::ready for development`
 1. `workflow::in dev`
 1. `workflow::blocked` (as necessary)
 1. `workflow::in review`
 1. `workflow::verification`
 1. `workflow::production`
 1. `Closed`

`workflow::planning breakdown` is driven by Product, but is a collaborative effort between Product, UX and Engineering. The steps for **planning breakdown** typically consists of:
 - Product defining or clarifying the problem statement. Product and UX will collaborate on `problem validation` as needed
 - UX providing designs (as necessary)
 - Engineering clarifying the issue description as stated, and refines and weights the issue once Product and UX have provided enough details to do so.

At any point, if an issue becomes blocked, it would be in the `workflow::blocked` status. If there is a blocking issue, it needs to be added to the issue description or linked to the issue with a 'blocked by' relationship.

`workflow::ready for development` means that an issue has been sufficiently [refined and weighted by Engineering](##how-engineering-refines-issues), upon request by Product and UX

`Closed` means that all code changes associated with the issue are fully enabled on gitlab.com. If it is being rolled out behind a feature flag, it means the feature flag is enabled for all users on gitlab.com.

#### "What do I work on next?"

Each member of the team can choose which issues to work on during a milestone by assigning the issue to themselves.  When the milestone is well underway and we find ourselves looking for work, we default to working **right to left** on the **issue board** by pulling issues in the right-most column. If there is an issue that a team member can help with on the board, they should do so instead of starting new work. This includes conducting code review on issues that the team member may not be assigned to, if they feel that they can add value and help move the issue along to completion.

Specifically, this means our work is prioritized in the following order:
 * Any verification on code that is in `~workflow::verification` or `workflow::production`
 * Conducting code reviews on issues that are `workflow::in review`
 * Unblocking anyone in `workflow::blocked` or `workflow::in dev` if applicable
 * Then, lastly, picking from the top of the `workflow::ready for development` for development column

The goal of this process is to reduce the amount of work in progress (WIP) at any given time. Reducing WIP forces us to "Start less, finish more", and it also reduces cycle time. Engineers should keep in mind that the DRI for a merge request is **the author(s)**, to reflect the importance of teamwork without diluting the notion that having a [DRI is encouraged by our values](/handbook/people-group/directly-responsible-individuals/#dris-and-our-values).

#### Weekly Issue Progress Updates

In order to keep our stakeholders informed of work in progress, we provide updates to issues either by updating the issue's **health status** and/or adding an **async issue update**.

##### Issue Health Status

For issues in the current milestone, we use the [Issue Health Status feature](https://docs.gitlab.com/ee/user/project/issues/#health-status) to indicate probability that an issue will ship in the current milestone. This status is updated by the DRI ([directly responsible individual](/handbook/people-group/directly-responsible-individuals/)) as soon as they recognize the probability has changed, or at minimum assessed on a weekly basis.

The following are definitions of the health status options:

- `On Track` - The issue has no current blockers, and is likely to be completed in the current milestone.
- `Needs Attention` - The issue is still likely to be completed in the current milestone, but there are setbacks or time constraints that could cause the issue to miss the release due dates.
- `At Risk` - The issue is highly **unlikely** to be completed in the current milestone, and will probably miss the release due dates.

If the health status changes from `On Track` to `Needs attention` or `At Risk`, we recommend that the DRI add a short comment stating the reason for the change, if there is no [async issue update](#async-issue-update) that captures this already.

##### Tracking Inactive Issues

As a general rule, any issues being actively worked on have one of the following workflow labels:

- `workflow::in dev`
- `workflow::in review`
- `workflow::verification`
- `workflow::production` (upon **closing** the issue)

The Health Status of these issues should be updated to:

1. `Needs Attention` - on the `1st` of the month.
1. `At Risk` - on the `8th` of the month.

EMs are responsible for manually updating the Health Status of [any inactive issues in the milestone](https://gitlab.com/groups/gitlab-org/-/issues?label_name%5B%5D=group%3A%3A<%= group.downcase %>&milestone_title=%23upcoming&not%5Blabel_name%5D%5B%5D=workflow%3A%3Acanary&not%5Blabel_name%5D%5B%5D=workflow%3A%3Ain+dev&not%5Blabel_name%5D%5B%5D=workflow%3A%3Ain+review&not%5Blabel_name%5D%5B%5D=workflow%3A%3Aproduction&not%5Blabel_name%5D%5B%5D=workflow%3A%3Astaging&not%5Blabel_name%5D%5B%5D=workflow%3A%3Averification&page=2&scope=all&state=opened) accordingly.
